
let traineer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends:{
		kanto: ['Brock', 'Misty'],
    	hoenn: ['May', 'Max'],
	},
	talk: function(){
		console.log(this.pokemon[0]+ "! I choose you!");
	}
}
	console.log(traineer);
	console.log("Result of dot Notation: ");
	console.log(traineer.name);

	console.log("Result of square bracket Notation: ");
	console.log(traineer['pokemon']);

	console.log("Result of talk method: ");
	traineer.talk();


function Pokemon(name, level){
		
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		
		this.tackle = function(targetPokemon){

			targetPokemon.pokemonHealth -= this.pokemonAttack;

			console.log(this.pokemonName + " tackled " +  targetPokemon.pokemonName);
			console.log(targetPokemon.pokemonName+"'s health is now reduced to "+targetPokemon.pokemonHealth);

		if (targetPokemon.pokemonHealth <= 0) {
			targetPokemon.fainted();
		}
	}

		this.fainted = function(){
			console.log(this.pokemonName + " fainted!");
		}
	}

	

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
	let geodude = new Pokemon("Geodude", 8);
	console.log(geodude);
	let mewtwo = new Pokemon("Mewtwo", 100);
	console.log(mewtwo);

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewtwo.tackle(geodude);
	console.log(geodude);
	
	pikachu.tackle(mewtwo);
	console.log(mewtwo);
	


